let burger = document.body.querySelector(".header__burger")
burger.addEventListener("click", ()=>{
    burger.classList.toggle("open-menu")
    document.body.querySelector(".menu").classList.toggle("open-menu")
})