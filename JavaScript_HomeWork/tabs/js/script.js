let content = [...document.querySelectorAll(".tabs-content li")]
let main = document.querySelector(".tabs")

main.addEventListener("click", (event) => {
    content.forEach(el => {
        if (el.getAttribute('data-text-id') === event.target.getAttribute('data-text-id')){
            el.style.opacity = "1"
        }
        else {
            el.style.opacity = "0"
        }

    })
})
