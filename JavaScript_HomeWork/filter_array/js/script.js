function filterBy(array, type){
    let nullArr
    if(typeof (type) !== "string" || typeof (array) !== "object"){
        return console.log("Error")
    }
    
    if(type === "object" ) {
        nullArr = array.filter(item => item === null)
        return array.filter(item => typeof (item) !== type).concat(nullArr)
    }
    else{
        return array.filter(item => typeof (item) !== type)
    }
}
let nullCheck = null
let undefCheck

const arr = [12, undefCheck,'cvb', 23, nullCheck, undefined, {"name": name}, null, [34],"qwewwdd","wewe", null, null, 'cvb', false]
console.log(filterBy(arr, "object"));