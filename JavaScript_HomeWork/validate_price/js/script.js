function createField(priceTitle){
    document.body.insertAdjacentHTML("afterbegin",
        `<span class='price'></span><span class='close'></span>
              <div class='container'>
                <p>${priceTitle}</p><input class='field'>
              </div>
              <span class='error'></span>            
            `)
}
createField("Price")

let container = document.querySelector(".container")
let field = document.querySelector("input")
let error = document.querySelector(".error")
let price = document.querySelector(".price")
let close = document.querySelector(".close")

field.addEventListener("blur", () => {
    if ( field.value == "" || parseInt(field.value) < 0 || parseInt(field.value) === -0 || field.value != parseInt(field.value)) {
        field.style.background = "red"
        container.querySelector("p").style.color = "black"
        field.style.color = "white"
        error.innerText = "Input correct price"
        price.innerText = ""
        document.querySelector(".close-btn").remove()

    }
    else {
        if (document.querySelector(".close-btn") == undefined){
            price.innerText = "Current price: " + `${field.value}`
            close.insertAdjacentHTML("afterbegin", "<button class='close-btn'>X</button>")
        }
        else {
            price.innerText = "Current price: " + `${field.value}`
        }
        field.style.color = "green"
    }
})
field.addEventListener("focus", () => {
    field.style.background = "white"
    field.style.color = "black"
    error.innerText = ""
    // price.innerText = ""

})

close.addEventListener("click", (event)=>{
    if(event.target.classList.contains('close-btn')){
        document.querySelector(".close-btn").remove()
        price.innerText = ""
        field.value = ""
    }
})

