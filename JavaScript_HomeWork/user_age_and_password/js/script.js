function createNewUser(){
    let firstName = prompt("Input firstname of user"),
        lastName = prompt("Input lastname of user"),
        birthday = prompt("Input birthday of user in a 'dd.mm.yyyy' format")


    if (firstName === "" || lastName === "") {
        return false
    } else if (birthday.length !== 10) {
        return false
    } else {
        let user = {
            firstName,
            lastName,
            birthday,
            getLogin() {
                return (this.firstName[0] + this.lastName).toLowerCase()
            },
            getPassword(){
                return (this.firstName[0].toUpperCase() + this.lastName + this.birthday[0]+this.birthday[1]+this.birthday[2]+this.birthday[3])
            },
            setLastname(value) {

            },
            setFirstName(value) {

            },
            getAge() {
                let temp = this.birthday.split(".")
                if (typeof temp[2] !== "undefined") {
                    this.birthday = temp[2] + "." + temp[1] + "." + temp[0]
                    return ((new Date().getTime() - new Date(this.birthday)) / (24 * 3600 * 365 * 1000)) | 0;
                }
            }
        }
        Object.defineProperty(user, 'firstName', {
            writable: false
        })
        Object.defineProperty(user, 'lastName', {
            writable: false
        })
        return user
    }
}

let newUser = createNewUser()

console.log(newUser)
console.log(newUser.getLogin())


newUser.setFirstName("Andrew")
newUser.setLastname("Litvinenko")
console.log(newUser)
newUser.firstName = "asdasdds"
console.log(newUser.getAge())
console.log(newUser.getPassword())