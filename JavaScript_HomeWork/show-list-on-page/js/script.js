document.body.insertAdjacentHTML("afterbegin", "<ul></ul>")
function addList(liArray, parent = document.body){
    let newList = liArray.map((elem)=>{
        let list = document.createElement('li')
        list.innerText = `${elem}`
        list.style.color = "teal"
        return list
    })
    newList.forEach((elem) => {parent.append(elem)})
}

let array = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];
addList(array, document.body.querySelector("ul"))

