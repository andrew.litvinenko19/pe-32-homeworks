let defTheme = document.querySelector(".default-theme")
let changer = document.querySelector(".theme-changer")
function changeTheme(){
    if (localStorage.getItem("theme") === "default" || localStorage.getItem("theme") === null){
        defTheme.href = "css/style.css"
    }
    else if (localStorage.getItem("theme") === "explorer"){
        defTheme.href = "css/awesomeThemes/explorer.css"
    }
}
changer.addEventListener("click", () => {
    if (localStorage.getItem("theme") === null || localStorage.getItem("theme") === "default"){
        defTheme.href = "css/awesomeThemes/explorer.css"
        localStorage.setItem("theme", "explorer")
    }
    else if(localStorage.getItem("theme") === "explorer"){
        defTheme.href = "css/style.css"
        localStorage.setItem("theme", "default")
    }
})
changeTheme()