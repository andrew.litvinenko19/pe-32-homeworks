function calc(a, b, operation) {
    let answer
    switch (operation) {
        case "+":
            return answer = a + b
        case "-":
            return answer = a - b
        case "/":
            return answer = a / b
        case "*":
            return answer = a * b
        default:
            return false
    }
}

let flag = true
while (flag) {

    let firstVal = +prompt("Welcome in a calculator, please, input first value"),
        operation = prompt("Input operation with him, u can use only : +, -, *, / "),
        secondVal = +prompt("Input second value")

    if (firstVal == "" || secondVal == "" || operation === "") {
        console.log("One of the space stay empty, try again");
    }
    if (isNaN(firstVal) || isNaN(secondVal)) {
        console.log("U can use only digits, not letters or symbols, try again");
    } else {
        if (calc(firstVal, secondVal, operation) === false) {
            console.log("We have no those operation, repeat input");
        } else {
            console.log(`${firstVal}${operation}${secondVal} = ` + calc(firstVal, secondVal, operation))
            flag = false
        }
    }


}