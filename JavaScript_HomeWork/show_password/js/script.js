
function showPassword(eve){
    if(eve.target.classList.contains('fa-eye')){
        eve.target.classList.replace("fa-eye","fa-eye-slash")
        eve.target.parentElement.querySelector('input').setAttribute("type", "text")
    }
    else{
        eve.target.classList.replace("fa-eye-slash", "fa-eye")
        eve.target.parentElement.querySelector('input').setAttribute("type", "password")
    }
    return false
}

let mainForm = document.querySelector(".password-form")
mainForm.addEventListener('click', (event) => {
    if (event.target.classList.contains('icon-password')){
        showPassword(event)
    }
})

let sendBut = document.body.querySelector(".btn")
sendBut.insertAdjacentHTML("beforebegin", "<p class='error'></p>")
let error = document.querySelector(".error")
sendBut.addEventListener('click', (event) => {
    let fields = [...document.querySelectorAll("input")]
    event.preventDefault()
    if(fields.reduce((first,second) => first.value === second.value) && fields.every(elem => elem.value !== "")){
        error.innerText = ""
        alert("You are welcome")
    }
    else{
        error.innerText = "Нужно ввести одинаковые значения"
        document.querySelector("p").style.color = "red"
    }
})
let field = document.getElementById("main-field")
field.addEventListener('focus', () => {
    document.querySelector("p").remove()
})