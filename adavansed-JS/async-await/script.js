class FindOpponent{
    constructor(parent) {
        this._ELEMENTS = {
            parent,
            wrapper: document.createElement('div'),
            button: document.createElement('button'),
            title: document.createElement('p'),
            desc: document.createElement('p')
        }
    }
    render(){
        let{parent, wrapper, button, title, desc} = this._ELEMENTS
        wrapper.remove()
        title.innerText = "Вычислить тебя по IP?"
        button.innerText = 'Вычислить!'
        parent.style.display = 'flex'
        parent.style.justifyContent = 'center'
        wrapper.append(title, button, desc)
        parent.prepend(wrapper)
    }

    async handleClick(){
        let localIp = await fetch('https://api.ipify.org/?format=json', {method: 'GET'}).then(r =>  r.json())
        await this.findIPInfo(localIp)
    }

    async findIPInfo(userIp){
        let userInform = await fetch(`http://ip-api.com/json/${userIp.ip}?fields=continent,country,regionName,city,district,query`, {method: 'GET'}).then(r => r.json())
        console.log(userInform)
        let {city, continent, country, district} = {...userInform}
        this._ELEMENTS.desc.innerText = `Континент: ${continent}, \n Страна: ${country}, \n Город: ${city}, \n Район: ${district}`
    }

}

let find = new FindOpponent(document.querySelector('body'))
find.render()
document.querySelector('button').addEventListener('click', () => {
    find.handleClick()
})
