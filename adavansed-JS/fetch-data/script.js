// Согласен, три последних метода можно было вынести в отдельный класс, но я понял это уже когда доделал(а пока делал горел идеей которую нарисовал) и у  меня не хватало времени на это, если нужно - я без проблем переделаю

class Films{
    constructor(){
        this._ELEMENTS = {
            wrapper: document.createElement('div')
        }
    }
    render(parent, filmDesc){
        let {wrapper} = this._ELEMENTS
        filmDesc.sort((a, b) => {return b.episodeId - a.episodeId})
        filmDesc.forEach(film => {

        wrapper.insertAdjacentHTML('afterbegin',
            `<div class="film ${film.episodeId}">
                    <p class="ep-name ${film.episodeId}">"${film.name}" Episode: ${film.episodeId}</p>
                    <p>${film.openingCrawl}</p>
                  </div>`)
            parent.append(wrapper)
            document.querySelector('.film').style.border = '3px solid green'
            document.querySelector('.film').style.margin = '10px'
            document.querySelector('.film').style.padding = '15px'
        })

    }

    async getFilm(){
    const films = await fetch("https://ajax.test-danit.com/api/swapi/films", {method: "GET"}).then(r => r.json())
        await this.getFilmInfo(films)
    }

    async getFilmInfo(films){
        let shortInfo = [...films.map(item => {
        let {name, episodeId, openingCrawl, id} = {...item}
            return {id, name, episodeId, openingCrawl}
    })]
        await this.render(document.querySelector('.container'), shortInfo)
        await this.getCharacters(shortInfo)
    }

    async getCharacters(shortInfo){
        let allFilmInfo
        let id
         for (const film of shortInfo.reverse()) {
            allFilmInfo = await fetch(`https://ajax.test-danit.com/api/swapi/films/${film.id}`, {method: 'GET'}).then(r => r.json())
             id = film.id
        let promises = allFilmInfo.characters.map((charUrl) => {
            return fetch(`${charUrl}`, {method: "GET"}).then(r => r.json());
        })
        Promise.all(promises).then((resp) =>{
            this.filterCharacters(resp, document.getElementsByClassName(`ep-name ${id}`))
        })
        }
    }

    async filterCharacters(resp, pos){
    let people = [...resp.map((r) => {
        let {name, birthYear, gender} = {...r}
        return {name, birthYear, gender}
    })]
        await this.renderCharacters(people, pos)
    }

    renderCharacters(character, position){
        let [pos] = [...position]
        character.reverse().forEach((ch => {
            pos.insertAdjacentHTML('afterend', `<p>Name: ${ch.name} || Birthday:  ${ch.birthYear} || Sex: ${ch.gender}</p>`)
        }))
    }
}


let film  = new Films()
film.getFilm()
