const books = [
    {
        author: "Скотт Бэккер",
        name: "Тьма, что приходит прежде",
        price: 70
    },
    {
        author: "Скотт Бэккер",
        name: "Воин-пророк",
    },
    {
        name: "Тысячекратная мысль",
        price: 70
    },
    {
        author: "Скотт Бэккер",
        name: "Нечестивый Консульт",
        price: 70
    },
    {
        author: "Дарья Донцова",
        name: "Детектив на диете",
        price: 40
    },
    {
        author: "Дарья Донцова",
        name: "Дед Снегур и Морозочка",
    }
];
const ul = document.createElement('ul')
document.querySelector('#root').insertAdjacentElement('afterbegin', ul)

function showList(list) {
    list.forEach((elem, index) => {
        try {
            if (elem.author === undefined) {
                throw new Error(`В элементе № ${index + 1}: не указан автор книги!`);
            } else if (elem.name === undefined) {
                throw new Error(`В элементе  № ${index + 1}: не указано название книги!`);
            } else if (elem.price === undefined) {
                throw new Error(`В элементе  № ${index + 1}: не указана цена книги!`);
            } else {
                ul.insertAdjacentHTML("beforeend",
                    `<li>Название: ${elem.name}, Автор: ${elem.author}, Цена: ${elem.price}грн</li>`
                )
            }
        } catch (err) {
            console.log(err)
        }
    })
}

showList(books)


