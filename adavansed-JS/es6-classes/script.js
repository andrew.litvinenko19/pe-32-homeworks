class Employee {
    constructor(name, age, salary) {
        this.name = name
        this.age = age
        this.salary = salary
    }
    set name(value){
        if (value  <= 0){
            return  alert("this Name is too short")
        }
        this._name = value
    }
    get name(){
        return this._name
    }
    set age(value){
        if(value <= 0){
            return alert("age must be bigger then 0")
        }
        this._age = value
    }
    get age(){
        return this._age
    }
    set salary(value){
        this._salary = value
    }
    get salary(){
        return this._salary
    }
    showInfo(){
        console.log("Имя -", this.name + "Возраст -", this.age + "ЗП -" , this.salary)
    }
}

new Employee('Ivan' +"\n", 31 +"\n", '4000 euro').showInfo()

class Programmer extends Employee {
    constructor(name,age, salary, lang) {
        super(name,age, salary);
        this.lang = lang
    }
    set salary(value) {
        this._salary = value
    }
    get salary(){
        return this._salary
    }
    set lang(value){
        this._lang = value
    }
    get lang(){
        return this._lang
    }
    showInfo(){
        console.log("Имя -", this.name + "Возраст -", this.age + "ЗП -" , super.salary + "Языки -", this.lang)
    }
}

new Programmer('Toliya'+ "\n", 43 + "\n", '3200 dollars' + "\n",'C++, Java, Python, Pascal').showInfo()

new Programmer('Fedia'+ "\n", 12 + "\n", '200 dollars' + "\n",'Pascal').showInfo()

new Programmer('Antonie'+ "\n", 30 + "\n", '34500 dollars' + "\n",'C++, Java, Python, Pascal, Angular, Ruby, English,   Russian').showInfo()

