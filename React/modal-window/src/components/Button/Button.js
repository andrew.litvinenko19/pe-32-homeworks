import React, {Component} from 'react';
import './Button.scss'
class Button extends Component {
    render() {
        const {content, color, handleClick}= this.props
        return (
                <button className={'open-button'}
                        style={{background:color}}
                        onClick={handleClick}>{content}
                </button>
        );
    }
}
export default Button;