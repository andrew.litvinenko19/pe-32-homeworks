import React from 'react';
import CardList from "../CardList/CardList";

const Products = () => {
  return (
    <div>
      <h1 style={{color: "white"}}>Albums</h1>
      <CardList itProducts/>
    </div>
  );
};

export default Products;