import React, {useEffect, useState} from 'react';
import Button from "../Button/Button";
import Star from "../Star/Star";
import Modal from "../Modal/Modal";
import './Card.scss'

const Card = ({
                name,
                price,
                url,
                color,
                article,
                singer,
                year,
                cardInCart,
                cartList,
                setCartList,
                favorList,
                setFavorList,
              }) => {
  const [isClose, setIsClose] = useState(true)
  const [inFavorites, setInFavorites] = useState(false)
  const [inCart, setInCart] = useState(cardInCart || false)
  const [cardToken, setCardToken] = useState(null)

  useEffect(() => {
    setCardToken(article)
      console.log(article)
  }, [favorList, article])

  const openModal = () => {
    setIsClose(!isClose)
  }

  const setActive = (type) => {
    setIsClose(type)
  }

  const deleteFromCart = () => {
    const index = cartList.indexOf(cardToken)
    setCartList(cartList.splice(index, 1))
    setInCart(false)
    localStorage.removeItem("cartList")
    localStorage.setItem("cartList", JSON.stringify(cartList))
    setActive(true)
  }


  const addToStorage = (type, item) => {
    const index = favorList.indexOf(cardToken)
    if (type === 'cart') {
      if (!cartList.includes(item)) {
        setCartList(cartList.push(item))
        setInCart(true)
        localStorage.setItem("cartList", JSON.stringify(cartList))
      }
    } else {
      if (!favorList.includes(item)) {
        setFavorList(favorList.push(item))
        setInFavorites(true)
        localStorage.setItem(`favorList`, JSON.stringify(favorList))
      } else {
        setFavorList(favorList.splice(index, 1))
        setInFavorites(false)
        localStorage.removeItem("favorList")
        localStorage.setItem("favorList", JSON.stringify(favorList))
      }
    }
    setActive(true)
  }

  const toggleFavorites = (item) => {
    console.log('favorList', favorList);
    const index = favorList.indexOf(item);

    if (favorList.includes(item)) {
      setFavorList(favorList.splice(index, 1))
      setInFavorites(false)
      localStorage.removeItem("favorList")
      localStorage.setItem("favorList", JSON.stringify(favorList))
    } else {
      const newFavorList = [...favorList,item];
      setFavorList(newFavorList);
      setInFavorites(true)
      localStorage.setItem(`favorList`, JSON.stringify(newFavorList))
    }
  }

  const addToCart = (item) => {
  }

  const modal = (
    <Modal
      header={'Adding'}
      closeButton={true}
      text={"Add this Album to cart?"}
      action={<div>
        <button className='modal__button' onClick={() => addToStorage('cart', article)}>Yes
        </button>
        <button className='modal__button' onClick={() => setActive(true)}>No</button>
      </div>}
      hidden={isClose}
      setActive={setActive}
    />)

  const delModal = (
    <Modal
      header={'Delete'}
      closeButton={true}
      text={"Deleted this Album from cart?"}
      action={<div>
        <button className='modal__button' onClick={() => deleteFromCart()}>Yes
        </button>
        <button className='modal__button' onClick={() => setActive(true)}>No</button>
      </div>}
      hidden={isClose}
      setActive={setActive}
    />)

  return (
    <div className={'card'} style={{background: color}}>
      <img width={250} height={250} src={url} alt=""/>
      <h4 className={"card-title"}>"{name}"</h4>
      <p className={"card-price"}>Price: {price}</p>
      <div className={'card-desc'}>
        <p>Singer: {singer}</p>
        <p>Year: {year}</p>
      </div>
      <div className={'card-options'}>
        <Star
          handleClick={() => toggleFavorites(article)} inStorage={inFavorites}
        />
        {inCart && <Button content={'Delete'} color={'red'} article={article} handleClick={openModal}/>}
        {!inCart && <Button content={'ADD TO CART'} color={'black'} article={article} handleClick={openModal}/>}
      </div>
      {(!isClose && !inCart) && modal}
      {inCart && delModal}
    </div>
  );
}

export default Card;