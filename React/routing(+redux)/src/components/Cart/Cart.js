import React, {useEffect, useState} from 'react';
import CardList from "../CardList/CardList";
import './Cart.scss'
import axios from "axios";
import {useDispatch, useSelector} from "react-redux";


const Cart = () => {
  const cartList = useSelector(state => state.cartList.data)
  const cardList  = useSelector(state => state.cardList.data)
  const dispatch = useDispatch()

  useEffect(() => {
    axios('/cardsList.json')
      .then(r => dispatch({type: 'SET_PRODUCTS', payload:r.data}))
      .catch(err => console.log(err))
  }, [])

  const cart = cardList.filter(card => cartList.includes(card.article))

  return (
    <div className={'cart'}>
      <h1 className={'cart__title'}>Cart</h1>
      {cart.length <= 0 && <p className='no-items'>Your cart is empty</p>}
      <CardList itCart cardsInCart={cart}/>
    </div>
  );
}

export default Cart;