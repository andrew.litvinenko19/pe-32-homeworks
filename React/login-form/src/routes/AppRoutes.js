import React from 'react';
import {Redirect, Route, Switch} from "react-router-dom";
import Cart from "../components/Cart/Cart";
import Products from "../components/Products/Products";
import Favorites from "../components/Favorites/Favorites";

const AppRoutes = () => {
  return (
    <div>
      <Switch>
        <Redirect exact from='/' to='/products'/>
        <Route exact path='/products'>
          <Products/>
        </Route>
        <Route exact path='/cart'><Cart/></Route>
        <Route exact path='/favorites'><Favorites/></Route>
      </Switch>
    </div>
  );
}

export default AppRoutes;