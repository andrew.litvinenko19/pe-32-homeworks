//OUR SERVICES script//

document.body.querySelector(".services-block-nav").addEventListener("click", (event) => {
    [...document.querySelectorAll(".hidden-block")].forEach(elem => {
        if (elem.getAttribute('data-content-id') === event.target.getAttribute('data-tab-id')){
            elem.style.display = "block"
        }
        else {
            elem.style.display = "none"
        }
    })
})

// *LOAD MORE* button script//
function showHideBlock(eve, arrayElem, makeBigger){
    arrayElem.forEach(elem => {
        elem.toggleAttribute("hidden")
    })
    makeBigger.style.paddingBottom = "100px"
    moreButton.remove()
}
const moreButton = document.body.querySelector(".ex-btn")
moreButton.addEventListener("click", (event) => {
    let hiddenEl = [...document.querySelectorAll(".hidden-part")]
    showHideBlock(event, hiddenEl, document.querySelector(".work-gallery"))
})

const filterTabs = document.body.querySelector(".example-nav")
filterTabs.addEventListener("click", (event) => {
    const tabImages = [...document.querySelectorAll(".work-card")]
    for (let i = 0; i < tabImages.length; i++){
        if (event.target.getAttribute("data-type") === tabImages[i].getAttribute("data-img")){
            tabImages[i].style.display = "block"
            moreButton.remove()
            document.querySelector(".work-gallery").style.paddingBottom = "100px"
        }
        else{
            tabImages[i].style.display = "none"
        }
        if (event.target.getAttribute('data-type') === "all"){
            for (let i = 0; i < tabImages.length; i++){
                tabImages[i].style.display = "block"
                moreButton.remove()
            }
        }
    }
})

const swiper = new Swiper('.big-swiper', {
    effect: 'fade',
    loop:true,
    fadeEffect: {
        crossFade: true,
    },
    simulateTouch:false,
});
const miniSwiper = new Swiper('.mini-swiper',{
    slidesPerView: 3,
    slidesPerGroup: 1,
    loop:true,
    centeredSlides: true,
    navigation: {
        nextEl: ".swiper-button-next",
        prevEl: ".swiper-button-prev",
    },
    thumbs: {
        swiper: swiper,
    },
    simulateTouch:false,
    slideToClickedSlide: true,
})
